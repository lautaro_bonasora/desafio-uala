//
//  ContantsParameters.swift
//  Desafio Uala
//
//  Created by Lautaro Bonasora on 11/08/2019.
//  Copyright © 2019 Lautaro Bonasora. All rights reserved.
//

import Foundation

let BOOK_ENDPOINT = "https://qodyhvpf8b.execute-api.us-east-1.amazonaws.com/test/books"

let BOOK_ID = "id"
let BOOK_NAME = "nombre"
let BOOK_AUTHOR = "autor"
let BOOK_AVAILABILITY = "disponibilidad"
let BOOK_POPULARITY = "popularidad"
let BOOK_COVER_IMAGE = "imagen"

let DATA_NOT_AVAILABLE_MESSAGE = "Info No Disponible" 

let MAIN_TITLE = "Biblioteca Ualá"

