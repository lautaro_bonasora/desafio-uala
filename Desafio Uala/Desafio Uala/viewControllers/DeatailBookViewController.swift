//
//  DeatailBookViewController.swift
//  Desafio Uala
//
//  Created by Lautaro Bonasora on 11/08/2019.
//  Copyright © 2019 Lautaro Bonasora. All rights reserved.
//

import UIKit
import Kingfisher

class DeatailBookViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bookNameLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var popularityRateLabel: UILabel!
    @IBOutlet weak var availabilityStateLabel: UILabel!
    
    var authorName: String?
    var bookName: String?
    var popularityRate: Int?
    var availabilityState: String?
    var imageUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bookNameLabel.numberOfLines = 2
        setView()

        // Do any additional setup after loading the view.
    }
    
    func setView() {
        if let authorName = self.authorName, let bookName = self.bookName, let popularityRate = self.popularityRate, let availabilityState = self.availabilityState, let imageUrl = self.imageUrl {
            
            let url = URL(string: imageUrl)
            imageView.kf.setImage(with: url)
            authorNameLabel.text = authorName
            bookNameLabel.text = bookName
            popularityRateLabel.text = "Popularidad " + String(popularityRate)
            availabilityStateLabel.text = availabilityState
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
