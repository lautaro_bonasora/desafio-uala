//
//  BooksOverviewViewController.swift
//  Desafio Uala
//
//  Created by Lautaro Bonasora on 11/08/2019.
//  Copyright © 2019 Lautaro Bonasora. All rights reserved.
//

import UIKit

class BooksOverviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
 
    @IBOutlet weak var mainTable: UITableView!
    var books: [Book] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadBooks()
        
        mainTable.delegate = self
        mainTable.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.title = MAIN_TITLE
    }
    
    
    func loadBooks() {
        let bookService = BookService()
        
        bookService.getBookList { (myBooks) in
            self.books = myBooks
            self.mainTable.reloadData()
        }
    }



}

//tableViewSetting

extension BooksOverviewViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookCell", for: indexPath)
        
        if let bookCell = cell as? BookTableViewCell {
            let newBook = books[indexPath.row]
            bookCell.setCell(book: newBook)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let detailController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DeatailBookViewController") as? DeatailBookViewController {
            let bookSelected = books[indexPath.row]
            
            detailController.authorName = bookSelected.getAuthorName()
            detailController.availabilityState = bookSelected.getAvailabilityMessage()
            detailController.bookName = bookSelected.getBookName()
            detailController.popularityRate = bookSelected.getPopularityRateNumber()
            detailController.imageUrl = bookSelected.getImageUrl()
            self.navigationController?.pushViewController(detailController, animated: true)
        }
        
    }
    
}
