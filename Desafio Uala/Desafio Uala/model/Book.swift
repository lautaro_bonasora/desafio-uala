//
//  Book.swift
//  Desafio Uala
//
//  Created by Lautaro Bonasora on 11/08/2019.
//  Copyright © 2019 Lautaro Bonasora. All rights reserved.
//

import Foundation

class Book {
    private var id: String?
    private var name: String?
    private var author: String?
    private var isAvalaible: Bool?
    private var image: String?
    private var popularity: NSNumber?
    
    init(dictionary: [String:Any]) {
        
        self.id = dictionary[BOOK_ID] as? String
        self.name = dictionary[BOOK_NAME] as? String
        self.author = dictionary[BOOK_AUTHOR] as? String
        self.isAvalaible = dictionary[BOOK_AVAILABILITY] as? Bool
        self.image = dictionary[BOOK_COVER_IMAGE] as? String
        self.popularity = dictionary[BOOK_POPULARITY] as? NSNumber
    }
    
    func getBookName() -> String {
        if let name = self.name {
            return name
        }
        
        return DATA_NOT_AVAILABLE_MESSAGE
    }
    
    func getAuthorName() -> String {
        if let author = self.author {
            return author
        }
        
        return DATA_NOT_AVAILABLE_MESSAGE
    }
    
    func getAvailabilityMessage() -> String {
        if let available = self.isAvalaible{
            
            if available {
                return "Disponible"
            }
            
            return "No Disponible"
        }
        
        return DATA_NOT_AVAILABLE_MESSAGE
    }
    
    func getImageUrl() -> String {
        if let url = self.image {
            return url
        }
        
        return DATA_NOT_AVAILABLE_MESSAGE
    }
    
    func getPopularityRate() -> String {
        if let popularity = self.popularity {
            return String(popularity.intValue)
        }
        
        return DATA_NOT_AVAILABLE_MESSAGE
    }
    
    func getPopularityRateNumber() -> Int {
        if let popularity = self.popularity {
            return popularity.intValue
        }
        return 0
    }
}
