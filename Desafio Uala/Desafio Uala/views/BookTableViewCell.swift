//
//  BookTableViewCell.swift
//  Desafio Uala
//
//  Created by Lautaro Bonasora on 11/08/2019.
//  Copyright © 2019 Lautaro Bonasora. All rights reserved.
//

import UIKit
import Kingfisher

class BookTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var popularityLabel: UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var bookCoverImage: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    func setCell(book: Book) {
        bookTitleLabel.text = book.getBookName()
        authorNameLabel.text = book.getAuthorName()
        popularityLabel.text = "Popularidad " + book.getPopularityRate()
        availabilityLabel.text = book.getAvailabilityMessage()
        let imgUrl = URL(string:  book.getImageUrl())
        bookCoverImage.kf.setImage(with: imgUrl)
    }

}
