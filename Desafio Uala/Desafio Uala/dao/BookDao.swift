//
//  BookDao.swift
//  Desafio Uala
//
//  Created by Lautaro Bonasora on 11/08/2019.
//  Copyright © 2019 Lautaro Bonasora. All rights reserved.
//

import Foundation
import Alamofire

class BookDao {
    
    func getBooks( completion: @escaping ([Book]) -> Void){
        
        Alamofire.request(BOOK_ENDPOINT).responseJSON(completionHandler: {
            
            myResponse in
            
            var bookList : [Book] = []
            if let results = myResponse.value as? [[String: AnyObject]] {
                
                for bookDictionary in results {
                    let newBook = Book(dictionary: bookDictionary)
                    bookList.append(newBook)
                }
                completion(bookList)
            }
        })
    }
    
    
}
