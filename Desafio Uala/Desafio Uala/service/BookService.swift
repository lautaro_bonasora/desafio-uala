//
//  BookService.swift
//  Desafio Uala
//
//  Created by Lautaro Bonasora on 11/08/2019.
//  Copyright © 2019 Lautaro Bonasora. All rights reserved.
//

import Foundation

class BookService {
    
    func getBookList(completion: @escaping ([Book]) -> Void){
        let bookDAO = BookDao()
        
        return bookDAO.getBooks(completion: {
            myBooks in
            
            completion(myBooks.sorted(by: { $0.getPopularityRateNumber() > $1.getPopularityRateNumber()}))
        })
    }
}


